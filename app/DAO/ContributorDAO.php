<?php

namespace app\DAO;

use app\BO\Contributor;
use Vespula\Auth\Adapter\AdapterInterface;

class ContributorDAO extends DAO implements AdapterInterface
{

    /**
     * @param string $filter Column to filter by
     * @param string $value Targetet value
     * 
     * @return mixed array of FormElementContent objects if several results, one FormElementContent object else
     */
    public function find($filter, $value){
        $request = 'SELECT * FROM Contributor
                    WHERE '.$filter.' = :value;';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':value' => $value
        ]);
        $result = $stmt->fetchAll();
        $data = [];
        foreach ($result as $row) {
            $data[] = new Contributor($row);
        }
        switch (count($data)) {
            case 0 : return false;
                    break;
            case 1 : return $data[0];
                    break; 
            default : return $data;
        }
    }

    public function persist(Contributor $contributor) {
        if ($this->find('c_id', $contributor->getId()) !== false) {
            $update = true;
            $request = 'UPDATE Contributor SET
                            c_login = :c_login,
                            c_password = :c_password,
                            c_label = :c_label,
                            c_mail = :c_mail,
                            c_token = :c_token
                        WHERE c_id = :c_id;';
        } else {
            $update = false;
            $request = 'INSERT INTO Contributor VALUES (
                            c_login = :c_login,
                            c_password = :c_password,
                            c_label = :c_label,
                            c_mail = :c_mail,
                            c_token = :c_token
                        );';
        }
        $stmt = $this->getPDO()->prepare($request);
        $binds = [
            ':c_login' => $contributor->getLogin(),
            ':c_password' => $contributor->getPassword(),
            ':c_label' => $contributor->getLabel(),
            ':c_mail' => $contributor->getMail(),
            ':c_token' => $contributor->getToken()
        ];
        if ($update === true) {
            $binds[':c_id'] = $contributor->getId();
        }
        $stmt->execute($binds);
        return true;
    }

    public function authenticate(array $credentials)
    {
        $request = 'SELECT * FROM Contributor
                    WHERE c_login = :username;';

        $stmt = $this->getPDO()->prepare($request);
        $stmt->execute([
            ':username' => $credentials['username']
        ]);
        $result = $stmt->fetch();
        return password_verify($credentials['password'], $result['c_password']);

    }

    public function lookupUserData($username)
    {
        return $this->find('c_login', $username);
    }

    public function getError()
    {
        return 'Error';
    }
}