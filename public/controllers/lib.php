<?php

use PHPMailer\PHPMailer\PHPMailer;

function send_mail($user, $smtp_connector) {
    // Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);
	try {
	    $mail->isSMTP();   
	    $mail->Host       = $smtp_connector['smtp_host'];
	    $mail->SMTPAuth   = true;
	    $mail->Username   = $smtp_connector['smtp_user']; 
	    $mail->Password   = $smtp_connector['smtp_pass']; 
		$mail->SMTPSecure = 'ssl';  
		if ($smtp_connector['smtp_certs'] == 'on') {
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
		}
		//$mail->SMTPDebug  = 4;
	    $mail->Port       = $smtp_connector['smtp_port'];                                   
	    $mail->setFrom($smtp_connector['smtp_user'], 'Administration Openflow');
	    $mail->addAddress($user->getMail(), $user->getLogin());    
	    $mail->isHTML(true);                                 
	    $mail->Subject = 'Réinitialisation de votre mot de passe Openflow';
	    $mail->Body    = '<a href = "'.$GLOBALS['domain'].'/public/index.php?page=reset&token='.$user->getToken().'">Cliquez sur ce lien pour réinitialiser votre mot de passe</a>';

	    $mail->send();
	} catch (Exception $e) {
	    throw new Exception($e->getMessage());
	}
}

function generate_token()
{
    return sha1(uniqid(microtime(), true));
}

function check_passwords($password, $confirm) {
    $length = strlen($password);
    if ($length <= 7) {
        return 'Le mot de passe doit contenir un minimum de 8 caractères';
    } 
    //password input comparison
    $compare = strcmp($confirm, $password);
    if ($compare != 0) {
        return 'Les deux mots de passe doivent être identiques';
    }
    return true;
}

function error_redirect($error_code, $from) {
	$from = $from;
	$error_code = $error_code;
	$renderer = renderers\Provider::get_renderer('error');
	require __DIR__.'/ErrorController.php';
	die();
}

function page_exist($page) {
	$files = array_diff(scandir(__DIR__), array('..', '.'));
	$files = array_merge($files, array_diff(scandir(__DIR__.'/back-office'), array('..', '.')));
	return in_array(ucfirst($page).'Controller.php', $files);
}

function can_access($page, $USER) {
	if ($USER === null) {
		return true;
	}
	$can_access = [];
	$files = array_diff(scandir(__DIR__), array('..', '.', 'back-office'));
	if ($USER->getRole() === 'Admin') {
		$files = array_merge($files, array_diff(scandir(__DIR__.'/back-office'), array('..', '.')));
	}
	foreach ($files as $file) {
		if ($file !== 'lib.php') {
			$can_access[] = strtolower(str_replace('Controller.php', '', $file));
		}
	}
	return in_array($page, $can_access);
}