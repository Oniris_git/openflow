<?php

use app\DAO\ContributorDAO;
use League\Container\Container;
use Vespula\Auth\Adapter\Ldap;
use Vespula\Auth\Auth;
use app\Session;
use PHPMailer\PHPMailer\PHPMailer;

// require __DIR__.'/lib.php';

// $contributor_dao = new ContributorDAO($db_connector);

$args = [
    'username' => FILTER_SANITIZE_STRING,
    'password' => FILTER_SANITIZE_STRING,
    'token' => FILTER_SANITIZE_STRING,
    'reset' => FILTER_VALIDATE_EMAIL
];

$POST = filter_input_array(INPUT_POST, $args, false);

$ERROR = [
    'message' => true
];

// trying to login
if (isset($POST['username']) && isset($POST['password'])) {
    if (!$SESSION->verifyToken('login_admin', $POST['token'])) {
        $ERROR = [
            'message' => "Invalid Token"
        ];
    }
    $credentials = [
        'username' => $POST['username'],
        'password' => $POST['password'],
    ];
    try {
        $contributor = $contributor_dao->find('c_login', $credentials['username']);
    } catch (\PDOException $e) {
        $ERROR = [
            'message' => 'Veuillez transmettre l\'erreur suivante à un administrateur : '.$e->getMessage()
        ];
        goto rendering;
    }
    if ($contributor !== false) {
        //auth via database
        $role = 'contributor';
    } else {    
        //auth via LDAP
        $container = new Container();
        $container->add('session', new Session());
        $container->add('adapter', function () use ($ldap_connector) {
            $ldap = new Ldap(
                $ldap_connector['ldap_uri'],
                null,
                [
                    'basedn' => $ldap_connector['ldap_base_dn'],
                    'binddn' => $ldap_connector['ldap_bind_dn'],
                    'bindpw' => $ldap_connector['ldap_bind_pass'],
                    'filter' => $ldap_connector['ldap_filter'].'=%s'
                ],
                [
                    LDAP_OPT_PROTOCOL_VERSION => 3,
                    LDAP_OPT_REFERRALS => true
                ],
                [
                    'cn',
                    'sn',
                    'givenName',
                    'mail',
                    'description',
                    $ldap_connector['ldap_filter']
                ],
                $ldap_connector['ldap_port']
            );

            $ldap->setEscapeChars('\&!|=<>,+"\';()');
            return $ldap;
        });
        $SESSION = $container->get('session');
        $adapter = $container->get('adapter');
        $auth = new Auth($adapter, $SESSION);
        $role = 'user';
    }
    try {
        $auth->login($credentials);
    } catch (\Vespula\Auth\Exception $e) {
        $ERROR = [
            'message' => $e->getMessage(),
        ];
        goto rendering;
    }
}

if($auth->isValid()){
    if (strpos($from,'login')) {
        $form = 'home';
    }
    header('Location: ?page='.$from);
    exit();   
}else{
    if (isset($POST['username'])) {
        $ERROR['message'] = 'Veuillez vérifier vos valeurs de connexion';
    }
    $token = $SESSION->generateToken('login_admin');
}

//reset password
if (isset($POST['reset']) && trim($POST['reset']) !== '') {
    $user = $contributor_dao->find('c_mail', $POST['reset']);
    if ($user === false) {
        echo json_encode([
            'error',
            'Aucun compte contributeur n\'est associé à cette adresse mail. Si vous avez oublié votre mot de passe LDAP veuillez contacter le service informatique'
        ]);
        die();
    }
    $token = generate_token();
    $user->setToken($token);
    $contributor_dao->persist($user);
    try{
        send_mail($user, $smtp_connector);
    }catch(Exception $e){
        echo json_encode([
            'error',
            'Une erreur est survenue lors de l\'envoi du mail : '.$e->getMessage()
        ]);
        die();
    }
    echo json_encode([
            'success',
            'Un email a été envoyé à '.$user->getMail()
        ]);
        die();
}

rendering :
$renderer->header('Connexion')
            ->open_body([
                'div' => [
                    'class' => 'app-container'
                ],
            ])
            ->previous_page($from)
            ->error($ERROR['message'])
            ->login_form($token)
            ->close_body()
            ->footer()
            ->render();