<?php

require __DIR__.'/../app/Init.php';
require __DIR__.'/controllers/lib.php';
$args = [
    "page" => FILTER_SANITIZE_STRING
];
$GET = filter_input_array(INPUT_GET, $args, false);

if (!isset($from)) {
    $from = 'home';
}
$USER = null;
//rooting
if(isset($GET['page'])) {
    $page = $GET['page'];
    if (!page_exist($page)) {
        error_redirect('404', $from);
    }
    if ($auth->isAnon() || $SESSION->getUserdata() === null) {
        $can_access = ['home', 'login', 'success', 'error', 'reset', 'logout'];
        if (!in_array($page, $can_access)) {
            $from = $page;
            $page = 'login';
        }
    } else {
        $USER = $SESSION->getUserData();
        if (!can_access($page, $USER)) {
            error_redirect('401', $from);
        }
    }
} else {
    $page = 'home';
}
$renderer = renderers\Provider::get_renderer($page);
$controller = ucfirst($page).'Controller.php';   
// if (!file_exists(__DIR__.'/controllers/'.$controller)) {
//     $controller = 'ErrorController.php';
//     $error_code = '404';
// }
require __DIR__.'/controllers/'.$controller;