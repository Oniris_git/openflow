function reset_password() {
    var email = document.getElementById('reset')
    if (email.value.trim() !== '') {
        $.ajax({
            url : 'index.php?page=login',
            type : 'POST',
            data : 'reset='+email.value,
            dataType : 'json',
            success: function(data) {
                show_toast(data[0], data[1])
            }
        })
    }
}